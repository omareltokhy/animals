﻿using System;

public class Lion : Animal, IPreditor, ICLimber
{
    public override void MakeNoise()
    {
        Console.WriteLine("Rooooaaaarrr!");
    }
    public void Prey()
    {
        Console.WriteLine("I eat antilopes.");
    }
    public void Climb()
    {
        Console.WriteLine("I climb trees");
    }
}
