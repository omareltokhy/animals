﻿using System;

public class Shark : Animal, IPreditor
{
    private double length;

    public double Length { get => length; set => length = value; }

    public override void MakeNoise()
    {
        Console.WriteLine("BlubBlubBlub...");
    }
    public void Prey()
    {
        Console.WriteLine("I eat small fish");
    }
}
