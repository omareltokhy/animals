﻿using System;

public interface IPreditor
{
    public void Prey(string food)
    {
        Console.WriteLine("I eat " + food + ".");
    }
}