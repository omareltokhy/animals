﻿using System;

public abstract class Animal
{
    private string name;
    private double weight;

    public string Name { get => name; set => name = value; }
    public double Weight { get => weight; set => weight = value; }

    enum Biome
    {
        City,
        Mountains,
        Safari,
        Ocean,
        Desert
    }
    public void Present()
    {
        Console.WriteLine("My name is " + this.name + " and I weigh " + this.weight + "´kilograms.");
    }
    public virtual void Sleep(int hours)
    {
        Console.WriteLine("This animal sleeps " + hours + " hours a day.");
    }
    public abstract void MakeNoise();
}
