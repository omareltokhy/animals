﻿using System;

namespace AnimalClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            Lion Leo = new Lion();
            Leo.Name = "Leo";
            Leo.Weight = 120;

            Shark Sean = new Shark();
            Sean.Name = "Sean";
            Sean.Weight = 130;
            Sean.Length = 7;

            Leo.Present();
            Leo.MakeNoise();
            Leo.Sleep(16);
            Leo.Prey();
            Leo.Climb();

            Console.WriteLine("=====================================================");

            Sean.Present();
            Sean.MakeNoise();
            Sean.Sleep(6);
            Sean.Prey();
        }
    }
}
